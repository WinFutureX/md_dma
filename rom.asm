; rom start addr
	org	$0

; vdp command types
vramw:	equ	$40000000	; vram write
cramw:	equ	$C0000000	; cram write
vsramw:	equ	$40000010	; vsram write

; 68k side dma v2 (one step current)
; src: 68k source addr
; len: length
; dest: destination addr
; type: destination type
; syntax: dmarun src, len, dest, type
dmarun	macro
	move.l	#$94000000 + (((\2 >> 1) & $FF00) << 8) + $9300 + ((\2 >> 1) & $FF), (a0)
	move.l	#$96000000 + (((\1 >> 1) & $FF00) << 8) + $9500 + ((\1 >> 1) & $FF), (a0)
	move.w	#$9700 + ((((\1 >> 1) & $FF0000) >> 16) & $7F), (a0)
	move.l	#\4 + $80 + ((\3 << 16) & $3FFF0000) + ((\3 >> 14) & 3), (a0)
	endm

; 68k vector table
vectors:
	dc.l	$0
	rept	63
	dc.l	startup
	endr

; mega drive cartridge header
mdcart:	dc.b	"SEGA MEGA DRIVE "
	dc.b	"(C)KELSEY B 2019"
	dc.b	"STARRING THE AIRBUS A380 PASSENGER AIRCRAFT     "
	dc.b	"STARRING THE AIRBUS A380 PASSENGER AIRCRAFT     "
	dc.b	"GM MDDMAROM-00"
	dc.w	$0
	dc.b	"J               "
	dc.l	vectors
	dc.l	romend-1
	dc.l	$FF0000
	dc.l	$FFFFFF
	dc.l	$20202020
	dc.l	$20202020
	dc.l	$20202020
	dc.b	"                                                    "
	dc.b	"JUE             "

startup:
	suba.l	sp, sp		; clear/reset stack pointer
	move.w	#$2700, sr	; disable ints
	move.b	$A10001, d0
	andi.b	#$F, d0		; check console revision
	beq.s	initz80
	move.l	$100.w, $A14000	; satisfy tmss

initz80:
	move.w	#$100, $A11100
	move.w	#$100, $A11200

z80wait:
	btst	#$0, $A11100
	bne.s	z80wait
	lea	z80code, a1
	lea	$A00000, a2
	move.w	#z80end-z80code-1, d1

z80loop:
	move.b	(a1)+, (a2)+
	dbf	d1, z80loop
	bra.s	z80end
	
z80code:
	dc.b	$AF		; xor	a
	dc.b	$01, $D9, $1F	; ld	bc, 1fd9h
	dc.b	$11, $27, $00	; ld	de, 0027h
	dc.b	$21, $26, $00	; ld	hl, 0026h
	dc.b	$F9		; ld	sp, hl
	dc.b	$77		; ld	(hl), a
	dc.b	$ED, $B0	; ldir
	dc.b	$DD, $E1	; pop	ix
	dc.b	$FD, $E1	; pop	iy
	dc.b	$ED, $47	; ld	i, a
	dc.b	$ED, $4F	; ld	r, a
	dc.b	$D1		; pop	de
	dc.b	$E1		; pop	hl
	dc.b	$F1		; pop	af
	dc.b	$08		; ex	af, af'
	dc.b	$D9		; exx
	dc.b	$C1		; pop	bc
	dc.b	$D1		; pop	de
	dc.b	$E1		; pop	hl
	dc.b	$F1		; pop	af
	dc.b	$F9		; ld	sp, hl
	dc.b	$F3		; di
	dc.b	$ED, $56	; im	1
	dc.b	$36, $E9	; ld	(hl), e9h
	dc.b	$E9		; jp	(hl)

z80end:
	move.w	#$0, $A11100
	move.w	#$0, $A11200

initvdp:
	lea	$C00004, a0
	lea	$C00000, a1
	move.w	(a0), d0
	clr.l	d0
	clr.l	d1
	move.l	#$80548154, (a0)
	move.l	#$82308330, (a0)
	move.l	#$84078578, (a0)
	move.l	#$86008700, (a0)
	move.l	#$88008900, (a0)
	move.l	#$8ADF8B00, (a0)
	move.l	#$8C898D00, (a0)
	move.l	#$8E008F02, (a0)
	move.l	#$90019200, (a0)
	move.l	#$93009400, (a0)
	move.l	#$95009700, (a0)

clearcram:
	move.l	#$C0000000, (a0)
	moveq	#$3F, d0

clearcramloop:
	move.l	d1, (a1)
	dbf	d0, clearcramloop

clearvram:
	move.l	#$40000000, (a0)
	move.w	#$3FFF, d0

clearvramloop:
	move.l	d1, (a1)
	dbf	d0, clearvramloop

clearvsram:
	move.l	#$40000010, (a0)
	moveq	#$13, d0

clearvsramloop:
	move.l	d1, (a1)
	dbf	d0, clearvsramloop

silencepsg:
	lea	$C00011, a3
	move.b	#$9F, (a3)	; silence channel 1
	move.b	#$BF, (a3)	; silence channel 2
	move.b	#$DF, (a3)	; silence channel 3
	move.b	#$FF, (a3)	; silence channel 4

clearram:
	moveq	#0, d2
	move.l	d2, a4
	move.w	#$3FFF, d3

clearramloop:
	move.l	d2, -(a4)
	dbf	d3, clearramloop

initjoy:
	move.b	#$40, $A10009	; controller port 1
	move.b	#$40, $A1000B	; controller port 2
	move.b	#$40, $A1000D	; ext port

main:
	; vram
	dmarun	plane, psize, $0, vramw
	; cram
	dmarun	cramd, csize, $0, cramw
		
	bra.s	*

plane:	incbin	"plane_vram.bin"
psize	equ	(* - plane) - 1

cramd:	incbin	"plane_cram.bin"
csize	equ	(* - cramd) - 1

romend	end			; end of rom